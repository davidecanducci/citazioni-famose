DROP TABLE IF EXISTS quotation_tag;
DROP TABLE IF EXISTS quotation;
DROP TABLE IF EXISTS persona;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS user;

CREATE TABLE tag (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	label VARCHAR(16) NOT NULL,
	last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`exists` TINYINT NOT NULL DEFAULT 1
);

CREATE TABLE category (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	label VARCHAR(32) NOT NULL,
	image VARCHAR(128) NOT NULL DEFAULT "/images/category/default.png",
	last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`exists` TINYINT NOT NULL DEFAULT 1
);

CREATE TABLE persona (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(48) NOT NULL,
	image VARCHAR(128) NOT NULL DEFAULT "/images/people/default.png",
	category INTEGER,
	last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`exists` TINYINT NOT NULL DEFAULT 1,
	FOREIGN KEY (category) REFERENCES category (id)
);

CREATE TABLE quotation (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	audio VARCHAR(256),
	text VARCHAR(2048) NOT NULL,
	persona INTEGER,
	views INTEGER NOT NULL DEFAULT 0,
	shares INTEGER NOT NULL DEFAULT 0,
	last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`exists` TINYINT NOT NULL DEFAULT 1,
	FOREIGN KEY (persona) REFERENCES persona (id)
);

CREATE TABLE quotation_tag (
	quotation INTEGER,
	tag INTEGER,
	last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`exists` TINYINT NOT NULL DEFAULT 1,
	FOREIGN KEY (quotation) REFERENCES quotation (id),
	FOREIGN KEY (tag) REFERENCES tag (id),
	PRIMARY KEY (quotation, tag)
);

CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR (16),
    salt INTEGER,
	password CHAR (64)
);
