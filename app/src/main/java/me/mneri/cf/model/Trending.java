package me.mneri.cf.model;

public class Trending  implements Model{
    private String title;
    private String subtitle;

    public Trending(String title, String subtitle) {
        setTitle(title);
        setSubtitle(subtitle);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
