package me.mneri.cf.model;

import java.util.List;

/**
 * Created by senza on 24/06/2017.
 */

public class QuotationsModel implements Model {
    private int id;
    private String audio;
    private String text;
    private int persona;
    private int views;
    private int shares;
    private int exists;
    private List<Integer> tags; //TODO o TAGS?


    public QuotationsModel( int id , String audio, String text, int character, int views, int shares, List<Integer> tag, int exists) {
        setId(id);
        setAudio(audio);
        setText(text);
        setPersona(character);
        setViews(views);
        setShares(shares);
        setTags(tag);
        setExists(exists);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getPersona() {
        return persona;
    }

    public void setPersona(int persona) {
        this.persona = persona;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public int getExists() {
        return exists;
    }

    public void setExists(int exists) {
        this.exists = exists;
    }
}
