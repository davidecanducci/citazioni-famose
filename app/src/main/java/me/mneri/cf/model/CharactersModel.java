package me.mneri.cf.model;

/**
 * Created by senza on 24/06/2017.
 */

public class CharactersModel implements Model {

    private int id;
    private String name;
    private String image;
    private int category;
    private int exists;

    public CharactersModel(int id, String name, String image, int category,  int exists) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.category = category;
        this.exists = exists;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getExists() {
        return exists;
    }

    public void setExists(int exists) {
        this.exists = exists;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
