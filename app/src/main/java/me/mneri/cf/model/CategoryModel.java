package me.mneri.cf.model;

/**
 * Created by senza on 24/06/2017.
 */

public class CategoryModel  implements Model{
    private int id;
    private String label;
    private String image;
    private int exists;

    public CategoryModel( int id, String label, String image, int exists) {
        setId(id);
        setLabel(label);
        setImage(image);
        setExists(exists);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String name) {
        this.label = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getExists() {
        return exists;
    }

    public void setExists(int exists) {
        this.exists = exists;
    }
}
