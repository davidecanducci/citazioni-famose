package me.mneri.cf.model;

import java.util.ArrayList;

import me.mneri.cf.ResponseModel.ResponseModel;

/**
 * Created by senza on 01/07/2017.
 */

public class TagResponseModel extends ResponseModel {

    ArrayList<CategoryModel> rows;
    public TagResponseModel(int status ,int date, ArrayList <CategoryModel> rows ) {
        super(status, date);

    }
    public ArrayList<CategoryModel> getRows() {
        return rows;
    }

    public void setRows(ArrayList<CategoryModel> rows) {
        this.rows = rows;
    }
}
