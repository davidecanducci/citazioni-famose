package me.mneri.cf.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import me.mneri.cf.R;
import me.mneri.cf.activity.AudioActivity;
import me.mneri.cf.database.DatabaseCitazioniFamose;
import me.mneri.cf.model.Model;
import me.mneri.cf.model.QuotationsModel;


/**
 * Created by Davide Canducci on 25/03/2017.
 */

public class QuotationsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {


    private static final int QUOTATION_TYPE = 1;
    private static final int TRENDING_TYPE = 2;
    private static final String TAG = "MyAdapter";
    private ArrayList<Model> mItems;
    private Context mContext;

    static class QuotationViewHolder extends RecyclerView.ViewHolder {
        private View mBase;
        private TextView tvQuotation;
        private RelativeLayout rlQuote;


        QuotationViewHolder(View base) {
            super(base);
            mBase = base;
        }

        TextView getQuotationTextView() {
            if (tvQuotation == null)
                tvQuotation = (TextView) mBase.findViewById(R.id.tvTitleQuote);

            return tvQuotation;
        }


        RelativeLayout getRootViewCharacter(){
            if(rlQuote == null)
                rlQuote = (RelativeLayout) mBase.findViewById(R.id.rlQuote);

            return rlQuote;
        }
    }

    public QuotationsListAdapter(ArrayList<Model> items, Context context) {
        mItems = items;
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position) instanceof QuotationsModel ? QUOTATION_TYPE : TRENDING_TYPE;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout relativeLayout;
        RecyclerView.ViewHolder holder = null;
        Log.i(TAG," Quotation type");
        relativeLayout = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.quote_row_recycler, parent, false);
        holder = new QuotationsListAdapter.QuotationViewHolder(relativeLayout);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Model model = mItems.get(position);
        final QuotationsModel quotation =(QuotationsModel) model;
        QuotationsListAdapter.QuotationViewHolder personaViewHolder = (QuotationsListAdapter.QuotationViewHolder) holder;
        personaViewHolder.getQuotationTextView().setText( quotation.getText() );
        personaViewHolder.getRootViewCharacter().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        Intent intent = new Intent(mContext, AudioActivity.class);
                        intent.putExtra(DatabaseCitazioniFamose.CHARACTER_ID, quotation.getId());
                        mContext.startActivity(intent);
            }
        });

    }
}
