package me.mneri.cf.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import me.mneri.cf.R;
import me.mneri.cf.activity.CharactersListActivity;
import me.mneri.cf.activity.QuotationsListActivity;
import me.mneri.cf.database.DatabaseCitazioniFamose;
import me.mneri.cf.model.CharactersModel;
import me.mneri.cf.model.Model;
import me.mneri.cf.model.Trending;

/**
 * Created by Davide Canducci on 25/03/2017.
 */

public class CharactersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {


    private static final int CHARACTER_TYPE = 1;
    private static final int TRENDING_TYPE = 2;
    private static final String TAG = "MyAdapter";
    private ArrayList<Model> mItems;
    private Context mContext;

    static class PersonaViewHolder extends RecyclerView.ViewHolder {
        private View mBase;
        private TextView tvTitlePersona;
        private ImageView ivIconPersona;
        private RelativeLayout rlCharacter;


        PersonaViewHolder(View base) {
            super(base);
            mBase = base;
        }

        TextView getPersonaTextView() {
            if (tvTitlePersona == null)
                tvTitlePersona = (TextView) mBase.findViewById(R.id.tvTitlePersona);

            return tvTitlePersona;
        }

        ImageView getIvIconPersona(){
            if (ivIconPersona == null)
                ivIconPersona = (ImageView) mBase.findViewById(R.id.ivIconaCharacter);

            return ivIconPersona;
        }
        RelativeLayout getRootViewCharacter(){
            if(rlCharacter == null)
                rlCharacter = (RelativeLayout) mBase.findViewById(R.id.rlCharacter);

            return rlCharacter;
        }


    }

    static class TrendingViewHolder extends RecyclerView.ViewHolder {
        private View mBase;
        private TextView tvTitleTrending;
        private TextView tvSubtitleTrending;

        public TrendingViewHolder(View base) {
            super(base);
            mBase = base;
        }

        TextView getTvTitleTrending(){
            if (tvTitleTrending == null)
                tvTitleTrending = (TextView) mBase.findViewById(R.id.tvTitleTrending);

            return tvTitleTrending;
        }

        TextView getTvSubtitleTrending(){
            if (tvSubtitleTrending == null)
                tvSubtitleTrending = (TextView) mBase.findViewById(R.id.tvSubtitleTrending);

            return tvSubtitleTrending;
        }
    }


    public CharactersListAdapter(ArrayList<Model> items, Context context) {
        mItems = items;
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position) instanceof CharactersModel ? CHARACTER_TYPE : TRENDING_TYPE;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout relativeLayout;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case CHARACTER_TYPE:
                Log.i(TAG," Persona type");
                relativeLayout = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.characters_row_recycler, parent, false);
                holder = new CharactersListAdapter.PersonaViewHolder(relativeLayout);
                return holder;
            case TRENDING_TYPE:
                Log.i(TAG, "Trending type");
                relativeLayout = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_row_recycler_main, parent, false);
                holder = new CharactersListAdapter.TrendingViewHolder(relativeLayout);
                return holder;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Model model = mItems.get(position);

        switch (holder.getItemViewType()) {
            case CHARACTER_TYPE:
                final CharactersModel characters =(CharactersModel) model;
                CharactersListAdapter.PersonaViewHolder personaViewHolder = (CharactersListAdapter.PersonaViewHolder) holder;
                personaViewHolder.getPersonaTextView().setText( characters.getName() );
                personaViewHolder.getRootViewCharacter().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, QuotationsListActivity.class);
                        intent.putExtra(DatabaseCitazioniFamose.CHARACTER_ID, characters.getId());
                        mContext.startActivity(intent);
                     }
                });
                break;

            case TRENDING_TYPE:
                Trending trending = (Trending) model;
                CharactersListAdapter.TrendingViewHolder trendingViewHolder = (CharactersListAdapter.TrendingViewHolder) holder;
                trendingViewHolder.getTvTitleTrending().setText(trending.getTitle());
                trendingViewHolder.getTvSubtitleTrending().setText(trending.getSubtitle());

                break;
        }
    }
}
