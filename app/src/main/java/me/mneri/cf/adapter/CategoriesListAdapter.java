package me.mneri.cf.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import me.mneri.cf.R;
import me.mneri.cf.activity.CharactersListActivity;
import me.mneri.cf.database.DatabaseCitazioniFamose;
import me.mneri.cf.model.CategoryModel;
import me.mneri.cf.model.Model;
import me.mneri.cf.model.Trending;


public class CategoriesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int CATEGORY_TYPE = 1;
    private static final int TRENDING_TYPE = 2;
    private static final String TAG = "MyAdapter";
    private ArrayList<Model> mItems;
    private Context context;


    static class CategoryViewHolder extends RecyclerView.ViewHolder {
        private View mBase;
        private TextView tvTitleCategory;
        private ImageView ivIconCategory;
        private RelativeLayout rlCategory;



        CategoryViewHolder(View base) {
            super(base);
            mBase = base;
        }

        TextView getCategoryTextView() {
            if (tvTitleCategory == null)
                tvTitleCategory = (TextView) mBase.findViewById(R.id.tvTitleCategory);

            return tvTitleCategory;
        }

        ImageView getIvIconCategory(){
            if (ivIconCategory == null)
                ivIconCategory = (ImageView) mBase.findViewById(R.id.ivIconCategory);

            return ivIconCategory;
        }

        RelativeLayout getRootViewCategory(){
            if(rlCategory == null)
                rlCategory = (RelativeLayout) mBase.findViewById(R.id.rlCategory);

            return rlCategory;
        }

    }

    static class TrendingViewHolder extends RecyclerView.ViewHolder {

        private View mBase;
        private TextView tvTitleTrending;
        private TextView tvSubtitleTrending;
        private RelativeLayout rlTrending;

        public TrendingViewHolder(View base) {
            super(base);
            mBase = base;
        }

        TextView getTvTitleTrending(){
            if (tvTitleTrending == null)
                tvTitleTrending = (TextView) mBase.findViewById(R.id.tvTitleTrending);

            return tvTitleTrending;
        }

        TextView getTvSubtitleTrending(){
            if (tvSubtitleTrending == null)
                tvSubtitleTrending = (TextView) mBase.findViewById(R.id.tvSubtitleTrending);

            return tvSubtitleTrending;
        }

        RelativeLayout getRootVievTrending(){
            if(rlTrending == null)
                rlTrending = (RelativeLayout) mBase.findViewById(R.id.rlTrending);

            return rlTrending;
        }
    }


    public CategoriesListAdapter(ArrayList<Model> items, Context context) {
         mItems = items;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position) instanceof CategoryModel ? CATEGORY_TYPE : TRENDING_TYPE;
}

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout relativeLayout;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case CATEGORY_TYPE:
                Log.i(TAG," Category type");
                relativeLayout = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_row_recycler_main, parent, false);
                holder = new CategoryViewHolder(relativeLayout);
                return holder;
            case TRENDING_TYPE:
                Log.i(TAG, "Trending type");
                relativeLayout = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_row_recycler_main, parent, false);
                holder = new TrendingViewHolder(relativeLayout);
                return holder;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Model model = mItems.get(position);

        switch (holder.getItemViewType()) {
            case CATEGORY_TYPE:
                final CategoryModel category =(CategoryModel) model;
                CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
                categoryViewHolder.getCategoryTextView().setText( category.getLabel() );
                categoryViewHolder.getRootViewCategory().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, CharactersListActivity.class);
                        intent.putExtra(DatabaseCitazioniFamose.CATEGORY_ID, category.getId());
                        context.startActivity(intent);
                     }
                });
                break;

            case TRENDING_TYPE:
                Trending trending = (Trending) model;
                TrendingViewHolder trendingViewHolder = (TrendingViewHolder) holder;
                trendingViewHolder.getTvTitleTrending().setText(trending.getTitle());
                trendingViewHolder.getTvSubtitleTrending().setText(trending.getSubtitle());
                trendingViewHolder.getRootVievTrending().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, CharactersListActivity.class);

                        context.startActivity(intent);
                     }
                });
                break;
        }
    }
}
