package me.mneri.cf.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Davide Canducci on 08/04/2017.
 */

public class DatabaseCitazioniFamose  extends SQLiteOpenHelper {


    //
    private static final String DB_NAME = "db_citazioni_famose";
    private static final int DB_VERSION = 1;

    //
    public static final String TABLE_TAG = "_TAG";
    public static final String TABLE_CATEGORY = "_CATEGORIA";
    public static final String TABLE_CHARACTER = "_PERSONA";
    public static final String TABLE_QUOTATION = "_CITAZIONE";
    public static final String TABLE_QUOTATION_TAG = "_CITAZIONE_TAG";

    //tag column
    public static final String TAG_ID = "tagID";
    public static final String TAG_NAME = "name";
    //TODO chiamando il campo exist mi andava in crash. Forse èerchè è una parola riservata? si massimo conferma che è esattamente così
    //category table
    public static final String CATEGORY_ID = "categoryID";
    public static final String CATEGORY_NAME = "name";
    public static final String CATEGORY_IMAGE = "image";
    public static final String CATEGORY_EXISTS = "existe";

    //character table
    public static final String CHARACTER_ID = "characterID";
    public static final String CHARACTER_NAME = "name";
    public static final String CHARACTER_IMAGE = "image";
    public static final String CHARACTER_CATEGORY = "category";
    public static final String CHARACTER_EXISTS = "existe";

    //quotation table
    public static final String QUOTATION_ID = "quotationID";
    public static final String QUOTATION_AUDIO = "audio";
    public static final String QUOTATION_TEXT = "text";
    public static final String QUOTATION_CHARACTER = "character";
    public static final String QUOTATION_VIEWS = "views_number";
    public static final String QUOTATION_SHARES = "shares_number";
    public static final String QUOTATION_TRENDING = "trending";
    public static final String QUOTATION_EXISTS = "existe";

    //quotation_tags table
    public static final String TAG_QUOTATION_QUOTATION = "quotation";
    public static final String TAG_QUOTATION_TAG = "tag";



    private String TAG = "DatabaseCitazioniFamose";


    public DatabaseCitazioniFamose(Context context) {
        super(context, DB_NAME, null , DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "OnCreate database");

        //tabella tag
        String sql="";
        sql +="CREATE TABLE "+TABLE_TAG +"(";
        sql +=" "+TAG_ID+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,";
        sql +=" "+TAG_NAME+" TEXT NOT NULL";
        sql +=")";
        db.execSQL(sql);

        Log.i(TAG,"SQL CATEGORY \n "+sql.toString());

        //tabella category
        sql="";
        sql +="CREATE TABLE "+TABLE_CATEGORY +"(";
        sql +=" "+CATEGORY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,";
        sql +=" "+CATEGORY_NAME+" TEXT NOT NULL,";
        sql +=" "+CATEGORY_IMAGE+" TEXT NOT NULL,";
        sql +=" "+CATEGORY_EXISTS+" TEXT NOT NULL";
        sql +=")";
        db.execSQL(sql);

        Log.i(TAG,"SQL CATEGORY \n "+sql.toString());

        //tabella character
        sql="";
        sql +="CREATE TABLE "+TABLE_CHARACTER +"(";
        sql +=" "+CHARACTER_ID+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,";
        sql +=" "+CHARACTER_NAME+" TEXT NOT NULL,";
        sql +=" "+CHARACTER_IMAGE+" TEXT NOT NULL,";
        sql +=" "+CHARACTER_CATEGORY+" TEXT NOT NULL,";
        sql +=" "+CHARACTER_EXISTS+" TEXT NOT NULL,";
        sql +=" FOREIGN KEY ("+CHARACTER_CATEGORY+") REFERENCES "+TABLE_CATEGORY+"("+CATEGORY_ID+")";
        sql +=")";
        db.execSQL(sql);
        Log.i(TAG,"SQL CHARACTER \n "+sql.toString());

        //tabella quotation
        sql="";
        sql +="CREATE TABLE "+ TABLE_QUOTATION +"(";
        sql +=" "+QUOTATION_ID+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,";
        sql +=" "+QUOTATION_AUDIO+" TEXT NOT NULL,";
        sql +=" "+QUOTATION_TEXT+" TEXT NOT NULL,";
        sql +=" "+QUOTATION_CHARACTER+" INTEGER,";
        sql +=" "+QUOTATION_VIEWS+" INTEGER NOT NULL,";
        sql +=" "+QUOTATION_SHARES+" INTEGER NOT NULL,";
//        sql +=" "+QUOTATION_TRENDING+" INTEGER NOT NULL,";
        sql +=" "+QUOTATION_EXISTS+" TEXT NOT NULL,";
        sql +=" FOREIGN KEY ("+QUOTATION_CHARACTER+") REFERENCES "+TABLE_CHARACTER+"("+CHARACTER_ID+")";
        sql +=")";
        db.execSQL(sql);
        Log.i(TAG,"SQL QUOTATION \n "+sql.toString());

        //tabella quotation tag
        sql="";
        sql +="CREATE TABLE "+ TABLE_QUOTATION_TAG +"(";
        sql +=" "+TAG_QUOTATION_QUOTATION+" INTEGER NOT NULL,";
        sql +=" "+TAG_QUOTATION_TAG+" INTEGER NOT NULL,";
        sql +=" FOREIGN KEY ("+TAG_QUOTATION_QUOTATION+") REFERENCES "+TABLE_QUOTATION+"("+QUOTATION_ID+"),";
        sql +=" FOREIGN KEY ("+TAG_QUOTATION_TAG+") REFERENCES "+TABLE_TAG+"("+TAG_ID+"),";
        sql +=" PRIMARY KEY ("+TAG_QUOTATION_QUOTATION+","+TAG_QUOTATION_TAG+")";
        sql +=")";
        db.execSQL(sql);
        Log.i(TAG,"SQL QUOTATION \n "+sql.toString());



//        + " FOREIGN KEY ("+TASK_CAT+") REFERENCES "+CAT_TABLE+"("+CAT_ID+"));";

        //tabella character
//        sql="";
//        sql +="CREATE TABLE "+TABLE_CHARACTER +"(";
//        sql +=" id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,";
//        sql +=" name TEXT NOT NULL,";
//        sql +=" image TEXT NOT NULL";
//        sql +=")";
//        db.execSQL(sql);
//        String sql="";
//        sql +="CREATE TABLE "+Costants._TABLE_1 +"(";
//        sql +=" "+Costants._ID+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,";
//        sql +=" "+Costants._GUADAGNO_GIORNATA+" TEXT NOT NULL";
//        sql +=")";
//        db.execSQL(sql);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "AGGIORNAMENTO DATABASE");

    }
}