package me.mneri.cf.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;

import me.mneri.cf.model.CategoryModel;
import me.mneri.cf.model.CharactersModel;
import me.mneri.cf.model.Model;
import me.mneri.cf.model.QuotationsModel;

/**
 * Created by Davide Canducci on 08/04/2017.
 */

public class DatabaseAdapter {

    private DatabaseCitazioniFamose dbCitazioniFamose;
    private SQLiteDatabase database;

    public DatabaseAdapter(Context context) {
        dbCitazioniFamose = new DatabaseCitazioniFamose(context);
    }

    //apro il database
    public void open() throws SQLException {
        database = dbCitazioniFamose.getWritableDatabase();
    }


    //chiudo il database
    public void close() {
        dbCitazioniFamose.close();
    }
//
//    public long updateContabilita (Contabilita contabilita){
//
//        String mese = ""+contabilita.getMese();
//        String anno = ""+contabilita.getAnno();
//        ContentValues values= new ContentValues();
//        values.put(Costants._RICAVI, contabilita.getRicavi());
//        values.put(Costants._RICAVI_DA_VIAGGI_C_TERZI, contabilita.getRicaviDaViaggiCTerzi());
//        values.put(Costants._RITIRI_GRUPPO_AMADORI, contabilita.getRitiriGruppoAmadori());
//        values.put(Costants._COSTI_VARIABILI, contabilita.getCostiVariabili());
//        values.put(Costants._CARBURANTI, contabilita.getCarburanti());
//        values.put(Costants._MANUTENZIONE_ORDINARIA, contabilita.getManutenzioneOrdinaria());
//        values.put(Costants._OFFICINA, "" + contabilita.getOfficina());
//        values.put(Costants._CARROZZERIA, "" + contabilita.getCarrozzeria());
//        values.put(Costants._GOMMISTA, "" + contabilita.getGommista());
//        values.put(Costants._FRIGORISTA, "" + contabilita.getFrigorista());
//        values.put(Costants._ELETTRAUTO, "" + contabilita.getElettrauto());
//        values.put(Costants._AUTOSTRADA, "" + contabilita.getAutostrada());
//        values.put(Costants._ALTRI_COSTI_VARIABILI, "" + contabilita.getAltriCostiVariabili());
//        values.put(Costants._PRIMO_MARGINE_CONTRAENTE, "" + contabilita.getPrimoMargineContraente());
//        values.put(Costants._COSTI_FISSI_SPECIFICI, "" + contabilita.getCostiFissiSpecifici());
//        values.put(Costants._AMMORTAMENTI, "" + contabilita.getAmmortamenti());
//        values.put(Costants._ASSICURAZIONI, "" + contabilita.getAssicurazioni());
//        values.put(Costants._TASSE_DI_PROPRIETA, "" + contabilita.getTasseDiProprieta());
//        values.put(Costants._COSTO_VETTORE, "" + contabilita.getCostoVettore());
//        values.put(Costants._COSTO_SECONDO_VETTORE, "" + contabilita.getCostoSecondoVettore());
//        values.put(Costants._ALTRI_COSTI_FISSI, "" + contabilita.getAltriCostiFissi());
//        values.put(Costants._ONERI_FINANZIARI, "" + contabilita.getOneriFinanziari());
//        values.put(Costants._SECONDO_MARGINE_CONTRAENTE, "" + contabilita.getSecondoMargineContraente());
//        values.put(Costants._RIMBORSO_PEDAGGI_AUSTRADALI, "" + contabilita.getRimborsoPedaggiAutostradali());
//        values.put(Costants._RIMBORSO_CARBON_TEX, "" + contabilita.getRimborsoCarbonTex());
//        values.put(Costants._RISULTATO_NETTO, "" + contabilita.getRisultatoNetto());
//        values.put(Costants._MESE, "" + contabilita.getMese());
//        values.put(Costants._ANNO, "" + contabilita.getAnno());
//        values.put(Costants._TARGA, "" + contabilita.getTarga());
//
//        String whereClause = Costants._MESE+" = ? AND "+Costants._ANNO+" = ? ";
//
//        String[] whereArgs = {mese, anno};
//        long success=database.update(Costants._TABLE_CONTABILITA, values, whereClause, whereArgs);
//
//        return success;
//    }

    public ArrayList<Model> getCategories() {
        ArrayList<Model> data = new ArrayList<>();
        String query;

        query = "SELECT * FROM " + DatabaseCitazioniFamose.TABLE_CATEGORY + " ";
        Cursor cursor = database.rawQuery(query, null);

        while (cursor.moveToNext()) {
            data.add(new CategoryModel(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)));
        }

        return data;
    }


    public ArrayList<Model> getCharacters(int categoryID){

        ArrayList<Model> data = new ArrayList<>();
        String query;
        String id = ""+categoryID;

        query = "SELECT * FROM "+DatabaseCitazioniFamose.TABLE_CHARACTER+" WHERE "+DatabaseCitazioniFamose.CHARACTER_CATEGORY+" = ? ";
        String[] selectionArgs = { id };
        Cursor cursor = database.rawQuery(query, selectionArgs);
        //se nel database esiste la row col mese che sto cercando
        if( cursor.getCount() > 0 ){
            while (cursor.moveToNext()) {
                data.add(new CharactersModel(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getInt(4)));
            }
        }
        return data;
    }

    public ArrayList<Model> getQuotations(int charactersID){

        ArrayList<Model> data = new ArrayList<>();
        String query;
        String id = ""+charactersID;

        query = "SELECT * FROM "+DatabaseCitazioniFamose.TABLE_QUOTATION+" WHERE "+DatabaseCitazioniFamose.QUOTATION_CHARACTER+" = ? ";
        String[] selectionArgs = { id };
        Cursor cursor = database.rawQuery(query, selectionArgs);
        //se nel database esiste la row col mese che sto cercando
        if( cursor.getCount() > 0 ){
            while (cursor.moveToNext()) {
                //TODO tira fuori la lista. dall'apposito database.
                data.add(new QuotationsModel(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getInt(4),cursor.getInt(5), null, cursor.getInt(6)));
            }
        }

        return data;
    }
    //TODO insert tags

    //insert categorie
    public boolean insertOrUpdateCategories(ArrayList<CategoryModel> categories){

        Log.i("DBINSERTCATEGORIES","Esiste la tabella categorie?"+ isTableExists(DatabaseCitazioniFamose.TABLE_CATEGORY));
        try{
            int i = 0;
            for (CategoryModel c : categories){

                ContentValues values= new ContentValues();
                values.put(DatabaseCitazioniFamose.CATEGORY_ID, c.getId());
                values.put(DatabaseCitazioniFamose.CATEGORY_NAME, c.getLabel());
                values.put(DatabaseCitazioniFamose.CATEGORY_IMAGE, c.getImage());
                values.put(DatabaseCitazioniFamose.CATEGORY_EXISTS, c.getExists());
                long l = database.insert(DatabaseCitazioniFamose.TABLE_CATEGORY, null, values);
                Log.i("DBINSERTCATEGORIES","Row "+i++ +  " Result "+l);

            }
        }catch (Exception e){
            return false;
        }
        return true;
    }


    //insert personaggi
    public boolean insertOrUpdateCharacters(ArrayList<CharactersModel> characters){
        Log.i("DBINSERTCATEGORIES","Esiste la tabella categorie?"+ isTableExists(DatabaseCitazioniFamose.TABLE_CHARACTER));
        try{
            int i = 0;
            for (CharactersModel c : characters){

                ContentValues values= new ContentValues();
                values.put(DatabaseCitazioniFamose.CHARACTER_ID, c.getId());
                values.put(DatabaseCitazioniFamose.CHARACTER_NAME, c.getName());
                values.put(DatabaseCitazioniFamose.CHARACTER_IMAGE, c.getImage());
                values.put(DatabaseCitazioniFamose.CHARACTER_CATEGORY, c.getCategory());
                values.put(DatabaseCitazioniFamose.CHARACTER_EXISTS, c.getExists());
                long l = database.insert(DatabaseCitazioniFamose.TABLE_CHARACTER, null, values);
                Log.i("DBINSERTCHARACTERS","Row "+i++ +  " Result "+l);

            }
        }catch (Exception e){
            return false;
        }
        return true;
    }

    //insert quotazione
    public boolean insertOrUpdateQuotations(ArrayList<QuotationsModel> quotations){
        Log.i("DBINSERTCATEGORIES","Esiste la tabella categorie?"+ isTableExists(DatabaseCitazioniFamose.TABLE_QUOTATION));
        try{
            int i = 0;
            for (QuotationsModel c : quotations){

                ContentValues values= new ContentValues();
                values.put(DatabaseCitazioniFamose.QUOTATION_ID, c.getId());
                values.put(DatabaseCitazioniFamose.QUOTATION_AUDIO, c.getAudio());
                values.put(DatabaseCitazioniFamose.QUOTATION_TEXT, c.getText());
                values.put(DatabaseCitazioniFamose.QUOTATION_CHARACTER, c.getPersona());
                values.put(DatabaseCitazioniFamose.QUOTATION_VIEWS, c.getViews());
                values.put(DatabaseCitazioniFamose.QUOTATION_SHARES, c.getShares());
                values.put(DatabaseCitazioniFamose.QUOTATION_EXISTS, c.getExists());
                //TODO inserisci lista tags in database
                long l = database.insert(DatabaseCitazioniFamose.TABLE_QUOTATION, null, values);
                Log.i("DBINSERTQuotations","Row "+i++ +  " Result "+l);

            }
        }catch (Exception e){
            return false;
        }
        return true;
    }

    //TODO insert tag-quotations

    public boolean isTableExists(String tableName) {
//        if(openDb) {
//            if(mDatabase == null || !mDatabase.isOpen()) {
//                mDatabase = getReadableDatabase();
//            }
//
//            if(!mDatabase.isReadOnly()) {
//                mDatabase.close();
//                mDatabase = getReadableDatabase();
//            }
//        }

        Cursor cursor = database.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                return true;
            }else {
                return false;
            }
        }
        return false;
    }
}
