package me.mneri.cf.ResponseModel;

import java.util.ArrayList;

import me.mneri.cf.model.QuotationsModel;

/**
 * Created by Davide Canducci on 29/04/2017.
 */

public class QuotationsResponseModel extends ResponseModel {

    ArrayList <QuotationsModel> rows;


    public QuotationsResponseModel(int success, int date, ArrayList <QuotationsModel> rows ){
        super(success,date);
        setQuotations(rows);
    }

    public ArrayList<QuotationsModel> getQuotations() {
        return rows;
    }

    public void setQuotations(ArrayList<QuotationsModel> rows) {
        this.rows = rows;
    }
}
