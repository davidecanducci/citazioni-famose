package me.mneri.cf.ResponseModel;

/**
 * Created by Davide Canducci on 29/04/2017.
 */

public class TagsResponseModel extends ResponseModel {

    private int id;
    private String name;

    public TagsResponseModel(int status, int date, int id, String name) {
        super(status, date);
        setId(id);
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
