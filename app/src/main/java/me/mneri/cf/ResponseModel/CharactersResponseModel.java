package me.mneri.cf.ResponseModel;

import java.util.ArrayList;

import me.mneri.cf.model.CharactersModel;

/**
 * Created by Davide Canducci on 29/04/2017.
 */

public class CharactersResponseModel extends ResponseModel {

    ArrayList <CharactersModel> rows;

    public CharactersResponseModel(int status, int date,  ArrayList <CharactersModel> rows) {
        super(status, date);
        setCharacters(rows);
     }

    public ArrayList<CharactersModel> getCharacters() {
        return rows;
    }

    public void setCharacters(ArrayList<CharactersModel> rows) {
        this.rows = rows;
    }
}
