package me.mneri.cf.ResponseModel;

import java.util.ArrayList;

import me.mneri.cf.model.CategoryModel;

/**
 * Created by Davide Canducci on 29/04/2017.
 */

public class CategoriesResponseModel extends ResponseModel {

    ArrayList <CategoryModel> rows;
    public CategoriesResponseModel(int status ,int date, ArrayList <CategoryModel> rows ) {
        super(status, date);

    }

    public ArrayList<CategoryModel> getRows() {
        return rows;
    }

    public void setRows(ArrayList<CategoryModel> rows) {
        this.rows = rows;
    }
}
