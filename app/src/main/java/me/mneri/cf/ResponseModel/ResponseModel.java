package me.mneri.cf.ResponseModel;

import java.io.Serializable;

/**
 * Created by Davide Canducci on 29/04/2017.
 */

public class ResponseModel implements Serializable {

    private int status;
    private int date;

    public ResponseModel(int status, int date) {
        this.status = status;
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }
}
