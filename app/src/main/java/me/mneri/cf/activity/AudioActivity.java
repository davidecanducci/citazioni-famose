package me.mneri.cf.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import me.mneri.cf.R;

/**
 * Created by senza on 29/07/2017.
 */

public class AudioActivity extends AppCompatActivity {
    private Button bPlay;
    private TextView tvDurationTime;
    public static boolean IS_RUNNING = false;
    public CountDownTimer countDown;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_activity);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "kenzo_regular.otf");
        tvDurationTime = (TextView) findViewById(R.id.tvDurationTime);
        tvDurationTime.setTypeface(font);
        bPlay = (Button) findViewById(R.id.bPlay);
        countDown = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tvDurationTime.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tvDurationTime.setText("done!");
            }
        };
        bPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cCurrentTime.start();
                if(!IS_RUNNING){
                    IS_RUNNING = true;
                    countDown.start();
                }else {
                    countDown.onFinish();
                    countDown.cancel();
                    IS_RUNNING = false;
                }
            }
        });
    }
}
