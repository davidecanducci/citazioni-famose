package me.mneri.cf.activity;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

import me.mneri.cf.R;
import me.mneri.cf.adapter.CharactersListAdapter;
import me.mneri.cf.database.DatabaseAdapter;
import me.mneri.cf.database.DatabaseCitazioniFamose;
import me.mneri.cf.model.CharactersModel;
import me.mneri.cf.model.Model;

/**
 * Created by Davide Canducci on 25/03/2017.
 */

public class CharactersListActivity extends AppCompatActivity {

    private int categoryID;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.characters_list_activity);
        mRecyclerView = (RecyclerView) findViewById(R.id.characters_recycler_view);

        // questa riga migliora le performances se i cambi nel content non
        // influiscono sulla dimensione del layout della RecyclerView
        mRecyclerView.setHasFixedSize(true);
        //TODO cazzo mi serve sta roba?
        String android_id1 = Settings.Secure.ANDROID_ID;
        String android_id2 = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("android_id","Settings.Secure.ANDROID_ID;"+ android_id1+"\n Settings.Secure.getString(this.getContentResolver()Settings.Secure.ANDROID_ID "+android_id2);
        // Google dice di usare un linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        categoryID = getIntent().getIntExtra(DatabaseCitazioniFamose.CATEGORY_ID, 0);
        DatabaseAdapter databaseAdapter = new DatabaseAdapter(CharactersListActivity.this);
        databaseAdapter.open();
        ArrayList<Model> characters = databaseAdapter.getCharacters(categoryID);
        databaseAdapter.close();
        if(characters.size() > 0){
            mAdapter = new CharactersListAdapter(characters, this);
            mRecyclerView.setAdapter(mAdapter);
        }
    }


}
