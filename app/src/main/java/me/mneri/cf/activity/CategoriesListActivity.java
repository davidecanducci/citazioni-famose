package me.mneri.cf.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import me.mneri.cf.R;
import me.mneri.cf.ResponseModel.CategoriesResponseModel;
import me.mneri.cf.ResponseModel.CharactersResponseModel;
import me.mneri.cf.ResponseModel.QuotationsResponseModel;
import me.mneri.cf.ServerFactory.JsonDeserializer;
import me.mneri.cf.Utils.Constants;
import me.mneri.cf.Utils.Utils;
import me.mneri.cf.adapter.CategoriesListAdapter;
import me.mneri.cf.database.DatabaseAdapter;
import me.mneri.cf.ServerFactory.IDownloadCallback;
import me.mneri.cf.ServerFactory.HttpAsyncTask;
import me.mneri.cf.ServerFactory.JsonPostRequest;
import me.mneri.cf.ServerFactory.IRequest;
import me.mneri.cf.ServerFactory.impl.BaseRequest;

public class CategoriesListActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private HttpAsyncTask mAsyncTask;
    private long lastUpdate;
    private IDownloadCallback<String> callbackTags = new IDownloadCallback<String>() {
        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onResult(String result) {

            try{
//                CategoriesResponseModel categoriesResponseModel= JsonDeserializer.deserializeCategories(result);
//                Log.i("aa", " "+categoriesResponseModel.getRows().get(0).getLabel());
                Log.i("aa", " memmele \n "+result);
            }
            catch (Exception e){

            }
        }
    };

    private IDownloadCallback<String> callbackCategories = new IDownloadCallback<String>() {
        @Override
        public void onError(Exception e) {      }

        @Override
        public void onResult(String result) {
            try{
                CategoriesResponseModel categoriesResponseModel= JsonDeserializer.deserializeCategories(result);
                DatabaseAdapter database = new DatabaseAdapter(CategoriesListActivity.this);
                //TODO
                /*
                *   if status 200 {
                *       Utils.setLastUpdateCategories(CategoriesListActivity.this, lastUpdate);
                *       if (firstAccess){
                *           database.insert
                *       }else{
                *           database.update
                *       }
                *   }else{
                *       non fare un cazzo perchè nulla è cambiato.
                *   }
                *
                * */
                database.open();
                database.insertOrUpdateCategories(categoriesResponseModel.getRows());
//                data.add(new Trending("Title","Subtitle"));
                mAdapter = new CategoriesListAdapter(database.getCategories(), CategoriesListActivity.this);
                database.close();
                mRecyclerView.setAdapter(mAdapter);

                //
                lastUpdate = Utils.getLastUpdateCharacters(CategoriesListActivity.this);
                downloadData(lastUpdate, callbackCharacters, Constants.API_REQUEST_CHARACTERS);
            }
            catch (Exception e){          }
        }
    };

    private IDownloadCallback<String> callbackCharacters = new IDownloadCallback<String>() {
        @Override
        public void onError(Exception e) {      }

        @Override
        public void onResult(String result) {
            try{
                CharactersResponseModel charactersResponseModel= JsonDeserializer.deserializeCharacters(result);
                Log.i("aa", " memmele \n "+result);
                //TODO
                /*
                *   if status 200 {
                *       Utils.setLastUpdateCharacters(CategoriesListActivity.this, lastUpdate);
                *       if (firstAccess){
                *           database.insert
                *       }else{
                *           database.update
                *       }
                *   }else{
                *       non fare un cazzo perchè nulla è cambiato.
                *   }
                *
                * */
                DatabaseAdapter database = new DatabaseAdapter(CategoriesListActivity.this);
                database.open();
                database.insertOrUpdateCharacters(charactersResponseModel.getCharacters());
                database.close();

                lastUpdate = Utils.getLastUpdateQuotations(CategoriesListActivity.this);
                downloadData(lastUpdate, callbackQuotations, Constants.API_REQUEST_QUOTATIONS);

            }
            catch (Exception e){            }
        }
    };

    private IDownloadCallback<String> callbackQuotations = new IDownloadCallback<String>() {
        @Override
        public void onError(Exception e) {      }

        @Override
        public void onResult(String result) {
            try{
                QuotationsResponseModel quotationsResponseModel = JsonDeserializer.deserializeQuotations(result);
                Log.i("aa", " memmele \n "+result);
                //TODO
                /*
                *   if status 200 {
                *       Utils.setLastUpdateQuotations(CategoriesListActivity.this, lastUpdate);
                *       if (firstAccess){
                *           database.insert
                *       }else{
                *           database.update
                *       }
                *   }else{
                *       non fare un cazzo perchè nulla è cambiato.
                *   }
                *
                * */
                DatabaseAdapter database = new DatabaseAdapter(CategoriesListActivity.this);
                database.open();
                database.insertOrUpdateQuotations(quotationsResponseModel.getQuotations());
                database.close();
                //

            }
            catch (Exception e){            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.main_recycler_view);
        // questa riga migliora le performances se i cambi nel content non
        // influiscono sulla dimensione del layout della RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // Google dice di usare un linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        lastUpdate = Utils.getLastUpdateCategories(CategoriesListActivity.this);
        downloadData(lastUpdate, callbackCategories, Constants.API_REQUEST_CATEGORIES);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public void downloadData(long lastUpdate, IDownloadCallback callback, String Url){
        cancelDownload();
        IRequest requestCategories = new JsonPostRequest(Url, new BaseRequest(lastUpdate));
        mAsyncTask = new HttpAsyncTask(requestCategories, callback);
        mAsyncTask.execute();
    }
    public void cancelDownload(){
        if(mAsyncTask != null){
            mAsyncTask.cancel(true);
        }
    }
}
