package me.mneri.cf.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import me.mneri.cf.R;
import me.mneri.cf.adapter.QuotationsListAdapter;
import me.mneri.cf.database.DatabaseAdapter;
import me.mneri.cf.database.DatabaseCitazioniFamose;
import me.mneri.cf.model.Model;

/**
 * Created by senza on 24/06/2017.
 */

public class QuotationsListActivity extends AppCompatActivity{
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int characterID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quotation_list_activity);
        characterID = getIntent().getIntExtra(DatabaseCitazioniFamose.CHARACTER_ID, 0);
        mRecyclerView = (RecyclerView) findViewById(R.id.quotations_recycler_view);

        // questa riga migliora le performances se i cambi nel content non
        // influiscono sulla dimensione del layout della RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // Google dice di usare un linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DatabaseAdapter databaseAdapter = new DatabaseAdapter(QuotationsListActivity.this);
        databaseAdapter.open();
        characterID = getIntent().getIntExtra(DatabaseCitazioniFamose.CHARACTER_ID, 0);
        ArrayList<Model> quotations = databaseAdapter.getQuotations(characterID);
        databaseAdapter.close();
        if(quotations.size() > 0){
            mAdapter = new QuotationsListAdapter(quotations, this);
            mRecyclerView.setAdapter(mAdapter);
        }

    }

}
