package me.mneri.cf.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by senza on 02/07/2017.
 */

public class Utils {

    private static final String PREF_LAST_UPDATE = "lastupdatePreference";
    private static final String LAST_UPDATE_CATEGORIES = "lastupdatecategories";
    private static final String LAST_UPDATE_CHARACTERS = "lastupdatecharacters";
    private static final String LAST_UPDATE_QUOTATIONS = "lastupdatequotations";

    //LAST_UPDATE_CATEGORIES
    public static void setLastUpdateCategories(Context ctx, long lastupdate){

        SharedPreferences sharedPref;
        sharedPref = ctx.getSharedPreferences(PREF_LAST_UPDATE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(LAST_UPDATE_CATEGORIES, lastupdate);
        editor.commit();

    }

    public static long getLastUpdateCategories(Context context){
        SharedPreferences settings;
        settings = context.getSharedPreferences(PREF_LAST_UPDATE, Context.MODE_PRIVATE);
        long lastupdate = settings.getLong(LAST_UPDATE_CATEGORIES, 1);
        return lastupdate;

    }

    //LAST_UPDATE_CHARACTERS
    public static void setLastUpdateCharacters(Context ctx, long lastupdate){

        SharedPreferences sharedPref;
        sharedPref = ctx.getSharedPreferences(PREF_LAST_UPDATE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(LAST_UPDATE_CHARACTERS, lastupdate);
        editor.commit();

    }
    public static long getLastUpdateCharacters(Context context){
        SharedPreferences settings;
        settings = context.getSharedPreferences(PREF_LAST_UPDATE, Context.MODE_PRIVATE);
        long lastupdate = settings.getLong(LAST_UPDATE_CHARACTERS, 1);
        return lastupdate;

    }

    //LAST_UPDATE_QUOTATION
    public static void setLastUpdateQuotations(Context ctx, long lastupdate){

        SharedPreferences sharedPref;
        sharedPref = ctx.getSharedPreferences(PREF_LAST_UPDATE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(LAST_UPDATE_QUOTATIONS, lastupdate);
        editor.commit();

    }
    public static long getLastUpdateQuotations(Context context){
        SharedPreferences settings;
        settings = context.getSharedPreferences(PREF_LAST_UPDATE, Context.MODE_PRIVATE);
        long lastupdate = settings.getLong(LAST_UPDATE_QUOTATIONS, 1);
        return lastupdate;

    }

}