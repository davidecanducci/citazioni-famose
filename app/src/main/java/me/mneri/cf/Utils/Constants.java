package me.mneri.cf.Utils;

/**
 * Created by senza on 24/06/2017.
 */

public interface Constants {

    String DOMAIN = "http://cheschifolafiga.it/";
    String API_REQUEST_CATEGORIES = DOMAIN + "categories";
    String API_REQUEST_CHARACTERS =  DOMAIN + "characters";
    String API_REQUEST_TAGS = DOMAIN + "tags";
    String API_REQUEST_QUOTATIONS = DOMAIN + "quotations";

}
