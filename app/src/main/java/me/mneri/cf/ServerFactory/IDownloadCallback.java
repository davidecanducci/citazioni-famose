package me.mneri.cf.ServerFactory;

/**
 * Created by senza on 10/06/2017.
 */

public interface IDownloadCallback<S> {
    void onError(Exception e);

    void onResult(S result);
}
