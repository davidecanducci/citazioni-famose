package me.mneri.cf.ServerFactory;

public class DeserializationException extends Exception {
    public DeserializationException(String message) {
        super(message);
    }

    public DeserializationException(String message, Exception cause) {
        super(message, cause);
    }
}
