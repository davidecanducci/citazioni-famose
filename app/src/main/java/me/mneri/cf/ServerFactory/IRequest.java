package me.mneri.cf.ServerFactory;

public interface IRequest {
    String getBody();
    String getMethod();
    String getUrl();
    String getContentType();
}
