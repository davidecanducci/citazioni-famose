package me.mneri.cf.ServerFactory;

import com.google.gson.Gson;

public class JsonPostRequest implements IRequest {
    private Object mObject;
    private String mUrl;

    public JsonPostRequest(String url, Object object) {
        mUrl = url;
        mObject = object;
    }

    @Override
    public String getBody() {
        Gson gson = new Gson();
        String userJson = gson.toJson(mObject);
        return userJson;
    }

    @Override
    public String getMethod() {
        return "POST";
    }

    @Override
    public String getUrl() {
        return mUrl;
    }

    @Override
    public String getContentType() {
        return "application/json";
    }
}
