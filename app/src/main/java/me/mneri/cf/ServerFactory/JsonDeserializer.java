package me.mneri.cf.ServerFactory;

import com.google.gson.Gson;

import me.mneri.cf.ResponseModel.CategoriesResponseModel;
import me.mneri.cf.ResponseModel.CharactersResponseModel;
import me.mneri.cf.ResponseModel.QuotationsResponseModel;

/**
 * Created by senza on 24/06/2017.
 */

public class JsonDeserializer {

    //deserializza le categorie
    public static CategoriesResponseModel deserializeCategories(String json)throws Exception{
        Gson gson = new Gson();
        CategoriesResponseModel categories = gson.fromJson(json, CategoriesResponseModel.class);
        return categories;
    }
    //deserializza i personaggi
    public static CharactersResponseModel deserializeCharacters(String json)throws Exception{
        Gson gson = new Gson();
        CharactersResponseModel characters = gson.fromJson(json, CharactersResponseModel.class);
        return characters;
    }

    //deserializza le citazioni
    public static QuotationsResponseModel deserializeQuotations(String json)throws Exception{
        Gson gson = new Gson();
        QuotationsResponseModel quotations = gson.fromJson(json, QuotationsResponseModel.class);
        return quotations;
    }
}
