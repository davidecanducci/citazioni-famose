package me.mneri.cf.ServerFactory;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class JsonResponseDeserializer<T> implements IDeserializer<T> {

    @Override
    public T deserialize(String source) throws DeserializationException {
        Log.i("ONRESULT", "son " + source);

        GsonBuilder gson = new GsonBuilder();
        Type collectionType = new TypeToken<Response<T>>(){}.getType();

        Response<T> response = gson.create().fromJson(source, collectionType);
        return response.rows;
    }
}
