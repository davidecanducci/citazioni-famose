package me.mneri.cf.ServerFactory;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

/*con T definiamo il tipo. E si dichiara proprio qui tra nome classe ed extends per sintassi.*/
public class HttpAsyncTask extends AsyncTask<String, Void, HttpAsyncTask.Result> {
    public static String TAG = "HttpAsyncTask";
    public static int READ_TIMEAOUT_HTTPS = 60000; // 10 secondi...
    public static int CONNECT_TIMEOUT_HTTPS = 60000; // 10 secondi...
    public static String METHOD_POST = "POST";
    public static String METHOD_GET = "GET";
    private IDownloadCallback mCallback;
//    private IDeserializer<T> mDeserializer;
    private IRequest mRequest;

    public HttpAsyncTask(IRequest request, IDownloadCallback callback) {
        mRequest = request;
//        mDeserializer = deserializer;
        mCallback = callback;
    }

    static class Result {
        public String mResultValue;
        public Exception mException;
        public Result(String resultValue) {
            mResultValue = resultValue;
            //non so se serve. Di default penso sia null
            mException = null;
        }
        public Result(Exception exception) {
            mException = exception;
        }
    }

    @Override
    protected Result doInBackground(String... params) {

        Result result = null;

        if (!isCancelled() && mRequest.getUrl() != null){
            String urlString = mRequest.getUrl();
            Log.i(TAG, "API: "+ urlString);
            try {
                URL url = new URL(urlString);
                String resultString = callApi(url);
                if (resultString != null) {
                    result = new Result(resultString);
                } else {
                    throw new IOException("No response received.");
                }
            }
            catch (Exception e){
                result = new Result(e);
                Log.i(TAG, "Exception: "+ e.toString());
                return result;
            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(Result r) {
        //se non ci sono stati errori durante il download / connessione col server allora procedoa  deserizlizzare il json o
        //comunque il risultato ricevuto. CHe potrebbe anche non essere un json ma magari una immagine in un flusso di byte...
        if(r.mException == null){

//                T result = mDeserializer.deserialize(r.mResultValue);
                mCallback.onResult(r.mResultValue);
        }else{
            mCallback.onError(r.mException);
        }
    }

    private String callApi(URL url) throws Exception {
        InputStream stream = null;
        HttpURLConnection connection = null;
        String result = null;
        connection = (HttpURLConnection) url.openConnection();
        connection.setReadTimeout(READ_TIMEAOUT_HTTPS);
        connection.setConnectTimeout(CONNECT_TIMEOUT_HTTPS);
        connection.setRequestProperty("Content-Type",mRequest.getContentType());
        connection.setRequestMethod(mRequest.getMethod());
        connection.setDoInput(true);
        connection.connect();

        if(mRequest.getMethod().equals(METHOD_POST)) {
            //scrivo il Json nell'output
            Log.i(TAG, "JsonRequest "+mRequest.getBody());
            Writer writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
            writer.write(String.valueOf(mRequest.getBody()));
            writer.close();
        }

        int responseCode = connection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new Exception("HTTP error code: " + responseCode);
        }
        stream = connection.getInputStream();
        try {
            if (stream != null) {
                result = readStream(stream);
            }
        } catch (Exception e) {
            throw e;
        }
        finally {
            // Close Stream and disconnect HTTPS connection.
            if (stream != null) {
                stream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    public String readStream(InputStream stream) throws Exception {
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = null;
        reader = new BufferedReader(new InputStreamReader(stream));
        String inputLine;

        while ((inputLine = reader.readLine()) != null) {
            buffer.append(inputLine + "\n");
        }
        if (buffer.length() == 0) {
            // Stream was empty. No point in parsing.
            Log.i(TAG, "jsoresponse length zero" );
        }

        String jsonResponse = buffer.toString();
        Log.i(TAG, "jsoresponse "+jsonResponse );
        return jsonResponse;
    }


    //    public <S> S deserialize(String source, Class<S> clazz) {
//        S instance = clazz.newInstance();
//        ...
//        return instance;
//    }
//
//    Category c = deserialize("asdadsad", Category.class);
}
