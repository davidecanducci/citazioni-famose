package me.mneri.cf.ServerFactory.impl;

public class BaseRequest {
    private long last_update;

    public BaseRequest(long lastUpdate){
        setLastUpdate(lastUpdate);
    }

    public long getLastUpdate() {
        return last_update;
    }

    public void setLastUpdate(long lastUpdate) {
        this.last_update = lastUpdate;
    }
}
