package me.mneri.cf.ServerFactory;

public interface IDeserializer<T> {
    T deserialize(String source) throws DeserializationException;
}
