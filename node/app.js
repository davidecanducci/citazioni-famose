var express     = require ('express')
    app         = express (),
    routes      = require ('./routes'),
    middlewares = require ('./middlewares'),
    path        = require ('path');

app.set ('views', path.join (__dirname, 'views'));
app.set ('view engine', 'jade');
middlewares.set (app);
routes.set (app);

if (app.get ('env') === 'development') {
    app.use (function (err, req, res, next) {
        res.status (err.status || 500);
        res.json ({
            status: err.status,
            time: req.time,
            message: err.message,
            rows: []
        });
    });
}

app.use (function (err, req, res, next) {
    res.status (err.status || 500);
    res.json ({
        status: err.status,
        time: req.time,
        rows: []
    });
});

module.exports = app;
