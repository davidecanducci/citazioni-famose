var q = require ('q');

function Promise (runnable) {
    var last,
        self = this;

    last = q.fcall (function () {
        var deferred = q.defer ();

        runnable (deferred.resolve, deferred.reject);

        return deferred.promise;
    });

    self.fail = function (err) {
        last.fail (err);
    };

    self.then = function (callback, errback) {
        last = last.then (function (data) {
            var deferred = q.defer ();

            callback (data, deferred.resolve, deferred.reject);

            return deferred.promise;
        });

        return self;
    };
}

module.exports = function (runnable) {
    if (!runnable) {
        runnable = function (resolve, reject) {
            resolve ();
        }
    }

    return new Promise (runnable);
}
