module.exports = function (req, res, next) {
    req.time = Math.round (new Date ().getTime () / 1000);
    next ();
};
