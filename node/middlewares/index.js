var express   = require ('express'),
    logger    = require ('morgan'),
    mysqlPool = require ('./mysql-pool'),
    path      = require ('path'),
    stylus    = require ('./stylus');
    time      = require ('./time');

module.exports.set = function (app) {
    app.use (time);
    app.use (logger ('dev'));
    app.use (mysqlPool);
    app.use (stylus);
    app.use (express.static (path.join (__dirname, '../public')));
};
