var path   = require ('path'),
    stylus = require ('stylus');

module.exports = stylus.middleware ({
    src: function (p) {
        return path.join (__dirname, '../stylesheets', p.replace(/^\/s\//, '').replace (/\.css$/, '.styl'));
    },
    dest: function (p) {
        return path.join (__dirname, '../public', p);
    },
    compile: function (str, path) {
        return stylus (str)
            .set ('filename', path)
            .set ('compress', true);
    }
});
