var config   = require ('../config'),
    finished = require ('on-finished'),
    mysql    = require ('mysql'),
    pool     = mysql.createPool ({
        host : config.DB_HOST,
        user : config.DB_USER,
        password : config.DB_PASSWORD,
        database : config.DB_NAME
    });

module.exports = function (req, res, next) {
    req.mysqlConnection = (function () {
        var connection;

        return function (callback) {
            if (connection) {
                callback (null, connection);
            } else {
                pool.getConnection (function (err, conn) {
                    if (err) {
                        callback (err, null);
                    } else {
                        connection = conn;
                        finished (res, function () {
                            connection.release ();
                        });

                        callback (null, conn);
                    }
                });
            }
        };
    }) ();

    next ();
};
