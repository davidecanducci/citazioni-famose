module.exports = function (req, res, next) {
    if (!req.body || req.body.last_update === undefined) {
        err = new Error ("Bad request");
        err.status = 400;
        next (err);
    }

    next ();
};
