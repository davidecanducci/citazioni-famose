var express    = require ('express'),
    lastUpdate = require ('../middlewares/last-update'),
    parser     = require ('body-parser'),
    promise    = require ('../modules/promise'),
    router     = express.Router ();

router.post('/', parser.json (), lastUpdate, function (req, res, next) {
    var connection;

    promise (function (resolve, reject) {
        req.mysqlConnection (function (err, conn) {
            if (err) {
                reject (err);
            } else {
                connection = conn;
                resolve ();
            }
        });
    }).then (function (_, resolve, reject) {
        connection.beginTransaction (function (err) {
            if (err) reject (err);
            else resolve ();
        });
    }).then (function (_, resolve, reject) {
        connection.query (' SELECT id, audio, text, persona, views, shares, `exists`' +
                          ' FROM quotation'                                           +
                          ' WHERE UNIX_TIMESTAMP(last_update) > ?', [req.body.last_update], function (err, rows) {
            if (err) reject (err);
            else resolve (rows);
        });
    }).then (function (rows, resolve, reject) {
        var i,
            ids = [];

        if (rows.length == 0) {
            resolve (rows);
            return;
        }

        for (i = 0; i < rows.length; i++) {
            rows[i].tags = [];
            ids.push (rows[i].id);
        }

        connection.query (' SELECT quotation, tag, UNIX_TIMESTAMP(last_update) AS last_update, `exists`' +
                          ' FROM quotation_tag'                                                          +
                          ' WHERE quotation IN (?)'                                                      +
                          ' AND UNIX_TIMESTAMP(last_update) > ?', [ids, req.body.last_update],
            function (err, relations) {
            var i,
                j;

            if (err) {
                reject (err);
            } else {
                for (i = 0; i < relations.length; i++) {
                    for (j = 0; j < rows.length; j++) {
                        if (relations[i].quotation == rows[j].id) {
                            rows[j].tags.push({
                                id: relations[i].tag,
                                exists: relations[i].exists
                            });

                            break;
                        }
                    }
                }

                resolve (rows);
            }
        });
    }).then (function (rows, resolve, reject) {
        connection.commit (function (err) {
            if (err) reject (err);
            else resolve (rows);
        });
    }).then (function (rows, resolve, reject) {
        res.json ({
            status: rows.length > 0 ? 200 : 304,
            server_time: req.time,
            rows: rows
        });
    }).fail (function (err) {
        if (connection) connection.rollback ();
        next (err);
    });
});

module.exports = router;
