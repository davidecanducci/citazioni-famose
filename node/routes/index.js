var characters = require ('./characters'),
    categories = require ('./categories'),
    lastUpdate = require ('../middlewares/last-update'),
    login      = require ('./login'),
    quotations = require ('./quotations'),
    tags       = require ('./tags');

module.exports.set = function (app) {
    app.use ('/categories', categories);
    app.use ('/characters', characters);
    app.use ('/quotations', quotations);
    app.use ('/tags', tags);
    app.use ('/login', login);

    app.use (function (req, res, next) {
        var err = new Error ('Not Found');

        err.status = 404;
        next (err);
    });
};
