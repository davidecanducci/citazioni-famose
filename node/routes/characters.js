var express    = require ('express'),
    lastUpdate = require ('../middlewares/last-update'),
    parser     = require ('body-parser'),
    promise    = require ('../modules/promise'),
    router     = express.Router ();

router.post('/', parser.json (), lastUpdate, function (req, res, next) {
    var connection;

    promise (function (resolve, reject) {
        req.mysqlConnection (function (err, conn) {
            if (err) {
                reject (err);
            } else {
                connection = conn;
                resolve ();
            }
        });
    }).then (function (_, resolve, reject) {
        connection.query (' SELECT id, name, image, category, `exists`' +
                          ' FROM persona'                               +
                          ' WHERE UNIX_TIMESTAMP(last_update) > ?', [req.body.last_update], function (err, rows) {
            if (err) reject (err);
            else resolve (rows);
        });
    }).then (function (rows, resolve, reject) {
        res.json ({
            status: rows.length > 0 ? 200 : 304,
            server_time: req.time,
            rows: rows
        });
    }).fail (function (err) {
        if (connection) connection.rollback ();
        next (err);
    });
});

module.exports = router;
