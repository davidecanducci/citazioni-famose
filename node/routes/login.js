var cookie     = require ('cookie-parser'),
    crypto     = require ('crypto'),
    express    = require ('express'),
    lastUpdate = require ('../middlewares/last-update'),
    parser     = require ('body-parser'),
    promise    = require ('../modules/promise'),
    router     = express.Router (),
    session    = require ('express-session');

function insert() {
    var hmac = crypto.createHmac ('sha256', 'hr7dTwx726a7aYU2');
    var salt = Math.round (Math.random () * Math.pow (2, 16));
    var encrypted = '';

    hmac.on ('readable', function () {
        var data = hmac.read ();

        if (data) encrypted += data.toString ('hex');
    });
    hmac.on ('end', function () {
        connection.query ('INSERT INTO user (username, salt, password) VALUES (?, ?, ?)',
            [username, salt, encrypted], function (err, rows) {
            if (err) reject (err);
            else resolve ();
        });
    });
    hmac.write (salt + password);
    hmac.end ();
}

router.get ('/', function (req, res, next) {
    res.render ('login', {
        title: 'Login'
    });
});

router.post ('/', parser.urlencoded ({extended: false}), function (req, res, next) {
    var connection;

    function accept (userId) {
        if (userId) req.session.user = {id: userId};
        res.redirect ('dashboard');
    }

    function refuse () {
        res.redirect ('login?failed=true');
    }

    if (req.session.user) {
        accept();
        return;
    }

    promise (function (resolve, reject) {
        req.mysqlConnection (function (err, conn) {
            if (err) {
                reject (err);
            } else {
                connection = conn;
                resolve ();
            }
        });
    }).then (function (_, resolve, reject) {
        var username = req.body.username,
            password = req.body.password;

        connection.query (' SELECT id, username, salt, password FROM user WHERE username = ?', username,
            function (err, rows) {
            if (rows.length === 0) {
                refuse ();
            } else {
                var encrypted = '',
                    hmac = crypto.createHmac ('sha256', 'hr7dTwx726a7aYU2'),
                    salt = rows[0].salt;

                hmac.on ('readable', function () {
                    var data = hmac.read ();
                    if (data) encrypted += data.toString ('hex');
                });
                hmac.on ('end', function () {
                    if (encrypted === rows[0].password) accept (rows[0].id);
                    else refuse ();
                });
                hmac.write (salt + password);
                hmac.end ();
            }
        });
    }).fail (function (err) {
        next (err);
    });
});

module.exports = router;
